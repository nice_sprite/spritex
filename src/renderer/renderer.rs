use gl::types::*;
use glutin::prelude::{GlConfig, GlDisplay};
use std::{ffi::{CStr, CString, c_void}, ptr::null, error::Error, path::Path, fs::File, num::NonZeroU32};
use std::io::Cursor;
use image::{io::Reader as ImageReader, EncodableLayout};
use image::error::ImageError;

use crate::renderer::math::{Matrix, Camera, Vec3, Vec4, Color4};
use log::{debug, info, trace, error};

/// from: https://stackoverflow.com/questions/38839258/converting-between-veci8-and-str
/// returns a null-terminated c-style string
macro_rules! cstr {
    ($str:expr) => {
        unsafe { CStr::from_bytes_with_nul_unchecked(concat!($str, "\0").as_bytes()).as_ptr() }
    };
}

pub struct Renderer {
    program: gl::types::GLuint,
    vao: gl::types::GLuint,
    vertex_buffer: gl::types::GLuint, 
    color_buffer: gl::types::GLuint,
    tex_buffer: gl::types::GLuint, // stores the uvs
    cmd: QuadDrawCmd,
    texture_test: Option<Texture2D>,
    camera: Camera,
}

impl Renderer {
    pub fn new<D: GlDisplay>(gl_display: &D) -> Self {
        unsafe {
            gl::load_with(|symbol| {
                let symbol = std::ffi::CString::new(symbol).unwrap();
                gl_display.get_proc_address(symbol.as_c_str()).cast()
            });

            if let Some(renderer) = get_gl_string(gl::RENDERER) {
                println!("Running on {}", renderer.to_string_lossy());
            }

            if let Some(version) = get_gl_string(gl::VERSION) {
                println!("OpenGL Version {}", version.to_string_lossy());
            }

            if let Some(shaders_version) = get_gl_string(gl::SHADING_LANGUAGE_VERSION) {
                println!("Shaders version on {}", shaders_version.to_string_lossy());
            }
 
            let texture_test = match load_texture2d(Path::new("/home/nice_sprite/Pictures/countryside.jpg")) {
                Ok(img) => {
                    info!("{} x {}", img.width, img.height);
                    Some(img)
                }
                Err(e) => {
                    error!("{:?}", e); 
                    None
                }
            };

            let mut cmd = QuadDrawCmd::new(100);
            cmd.add_quad(0.0, 0.5, 0.0, 0.5, Vec4 { x: 1.0, y: 1.0, z: 1.0, w: 1.0 }, None);


            let vertex_shader = create_shader(gl::VERTEX_SHADER, VERTEX_SHADER_SOURCE);
            let fragment_shader = create_shader(gl::FRAGMENT_SHADER, FRAGMENT_SHADER_SOURCE);

            let program = gl::CreateProgram();

            gl::AttachShader(program, vertex_shader);
            gl::AttachShader(program, fragment_shader);

            gl::LinkProgram(program);

            gl::UseProgram(program);

            gl::DeleteShader(vertex_shader);
            gl::DeleteShader(fragment_shader);

            let mut vao = std::mem::zeroed();
            gl::GenVertexArrays(1, &mut vao);
            gl::BindVertexArray(vao);

            let mut vertex_buffer = std::mem::zeroed();
            gl::GenBuffers(1, &mut vertex_buffer); 
            gl::BindBuffer(gl::ARRAY_BUFFER, vertex_buffer); gl::BufferData( gl::ARRAY_BUFFER, cmd.vertices.capacity() as _, cmd.vertices.as_ptr() as *const _, gl::STATIC_DRAW,); let mut color_buffer = std::mem::zeroed();
            gl::GenBuffers(1, &mut color_buffer);
            gl::BindBuffer(gl::ARRAY_BUFFER, color_buffer);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                cmd.colors.capacity() as _,
                cmd.colors.as_ptr() as *const _,
                gl::STATIC_DRAW,
            );

            let mut tex_buffer = std::mem::zeroed();
            gl::GenBuffers(1, &mut tex_buffer);
            gl::BindBuffer(gl::ARRAY_BUFFER, tex_buffer);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                cmd.coord.capacity() as _,
                cmd.coord.as_ptr() as *const _,
                gl::STATIC_DRAW,
            );

            gl::EnableVertexAttribArray(0 as gl::types::GLuint);
            gl::EnableVertexAttribArray(1 as gl::types::GLuint);
            gl::EnableVertexAttribArray(2 as gl::types::GLuint);

            Self {
                program,
                vao,
                vertex_buffer,
                color_buffer,
                tex_buffer,
                cmd,
                texture_test,
                camera: Camera::new(Vec3::new(0.0, 0.0, -5.0), 0.1, 100.0, 45.0),
            }
        }
    }

    pub fn draw(&self, width: NonZeroU32, height: NonZeroU32) {
        unsafe {
            gl::UseProgram(self.program);
            #[allow(unused_unsafe)]
            let mvp_id = gl::GetUniformLocation(self.program, cstr!("MVP"));

            let width  = width.get() as f32;
            let height = height.get() as f32;

            // create perspective matrix
            let perspective = Matrix::perspective_column_sequential(45.0, width/height, self.camera.z_near, self.camera.z_far);

            // create view matrix
            let view_matrix = Matrix::look_at();


            // create model matrix 

            


            gl::BindVertexArray(self.vao);

            gl::EnableVertexAttribArray(0);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vertex_buffer);
            gl::VertexAttribPointer( 0, 3, gl::FLOAT, gl::FALSE, 0, null());

            gl::EnableVertexAttribArray(1);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.color_buffer);
            gl::VertexAttribPointer(1, 3, gl::FLOAT, gl::FALSE, 0, null());

            gl::EnableVertexAttribArray(2);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.tex_buffer);
            gl::VertexAttribPointer(2, 2, gl::FLOAT, gl::FALSE, 0, null());

            gl::ClearColor(0.5, 0.1, 0.5, 0.5);
            gl::Clear(gl::COLOR_BUFFER_BIT);

            gl::DrawArrays(gl::TRIANGLES, 0, self.cmd.vertices.len() as i32 * 3i32);
            gl::DisableVertexAttribArray(0);
            gl::DisableVertexAttribArray(1);
            gl::DisableVertexAttribArray(2);
        }
    }

    pub fn resize(&self, width: i32, height: i32) {
        unsafe {
            gl::Viewport(0, 0, width, height);
        }
    }
    
}

impl Drop for Renderer {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteProgram(self.program);
            gl::DeleteBuffers(1, &self.vertex_buffer);
            gl::DeleteBuffers(1, &self.color_buffer);
            gl::DeleteBuffers(1, &self.tex_buffer);
            if let Some(texture) = self.texture_test.as_ref() {
                if let Some(handle) = texture.handle.as_ref() {
                    match handle {
                        BackendHandle::OpenGLHandle(handle) => gl::DeleteTextures(1, handle as *const GLuint),
                    }
                }
            }
            gl::DeleteVertexArrays(1, &self.vao);
        }
    }
}

unsafe fn create_shader(shader: gl::types::GLenum, source: &[u8]) -> gl::types::GLuint {
    let shader = gl::CreateShader(shader);
    gl::ShaderSource(
        shader,
        1,
        [source.as_ptr().cast()].as_ptr(),
        std::ptr::null(),
        );
    gl::CompileShader(shader);
    shader
}

fn get_gl_string(variant: gl::types::GLenum) -> Option<&'static CStr> {
    unsafe {
        let s = gl::GetString(variant);
        (!s.is_null()).then(|| CStr::from_ptr(s.cast()))
    }
}


enum BackendHandle {
    OpenGLHandle(gl::types::GLuint)
}

impl std::fmt::Display for BackendHandle {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            BackendHandle::OpenGLHandle(ogl) => {
                write!(f, "OpenGLHandle({})", ogl)
            }
        }
    }
}

impl std::fmt::Debug for BackendHandle {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            BackendHandle::OpenGLHandle(ogl) => {
                write!(f, "OpenGLHandle({})", ogl)
            }
        }
    }
}

pub struct Texture2D {
    handle: Option<BackendHandle>,
    width: u32,
    height: u32,
}

impl std::fmt::Display for Texture2D {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Texture2D {{ handle: {:?} width: {}, height: {} }}", self.handle, self.width, self.height)
    }
}


pub fn load_texture2d(file_path: &Path) -> Result<Texture2D, ImageError> {
    let img = ImageReader::open(file_path)?.decode()?.to_rgba8();
    //let img = img.to_rgba8();
    let width = img.width();
    let height = img.height();

    let mut tex_id: GLuint = 0;
    unsafe {
        gl::GenTextures(1, &mut tex_id);
        gl::BindTexture(gl::TEXTURE_2D, tex_id);
        gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGBA as GLint, width as i32, height as i32, 0, gl::RGBA, gl::UNSIGNED_BYTE, img.as_ptr() as *const c_void);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST as i32);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST as i32);
    }
    Ok(Texture2D { handle: Some(BackendHandle::OpenGLHandle(tex_id)), width, height })
}

struct QuadDrawCmd {
    max_quads: usize,
    num_quads: usize,
    vertices: Vec<Vec3>,
    colors: Vec<Vec4>,
    coord: Vec<Vec2>,
}

impl QuadDrawCmd {
    fn new(num_quads: usize) -> Self {
        Self {
            max_quads: num_quads,
            num_quads: 0,
            vertices: Vec::with_capacity(num_quads * 6),
            colors: Vec::with_capacity(num_quads * 6),
            coord: Vec::with_capacity(num_quads * 6),
        }
    }

    /// @return
    pub fn add_quad(&mut self, left: f32, right: f32, top: f32, bottom: f32, color: Vec4, uvs: Option<(Vec2, Vec2)> ) -> Option<usize> {
        if self.num_quads < self.max_quads {
            self.vertices.push(Vec3 { x: left, y: top, z: 0.0 });
            self.vertices.push(Vec3 { x: right, y: top, z: 0.0 });
            self.vertices.push(Vec3 { x: right, y: bottom, z: 0.0 });
            self.vertices.push(Vec3 { x: right, y: bottom, z: 0.0 });
            self.vertices.push(Vec3 { x: left, y: bottom, z: 0.0 });
            self.vertices.push(Vec3 { x: left, y: top, z: 0.0 });
            self.colors.append(&mut ([color].repeat(6)));
            if let Some((uv_min, uv_max)) = uvs {
                self.coord.push(uv_min);
                self.coord.push(Vec2{x: uv_max.x, y: uv_min.y});
                self.coord.push(uv_max);
                self.coord.push(uv_max);
                self.coord.push(Vec2{x: uv_min.x, y: uv_max.y});
                self.coord.push(uv_min);

            } else {
                self.coord.push(Vec2{x: 0.0, y: 0.0});
                self.coord.push(Vec2{x: 1.0, y: 0.0});
                self.coord.push(Vec2{x: 1.0, y: 1.0});
                self.coord.push(Vec2{x: 1.0, y: 1.0});
                self.coord.push(Vec2{x: 0.0, y: 1.0});
                self.coord.push(Vec2{x: 0.0, y: 0.0});

            }
            self.num_quads += 1;
            return Some(self.num_quads-1);
        }
        return None 
    }

    #[allow(unused)]
    pub fn clear(&mut self) {
        self.vertices.clear();
        self.colors.clear();
        self.coord.clear();
    }
}


const VERTEX_SHADER_SOURCE: &[u8] = b"
#version 330 core
layout(location = 0) in vec3 vertexPos;
layout(location = 1) in vec3 vertexColor;
layout(location = 2) in vec2 vertexUV;
out vec3 frag_color;
out vec2 uv;

uniform mat4 MVP;

void main() {
    gl_Position = MVP * vec4(position, 1.0);
    uv = vertexUV;
}
\0";

const FRAGMENT_SHADER_SOURCE: &[u8] = b"
#version 330 core

in vec3 fragColor;
in vec2 uv;
out vec3 color;

uniform sampler2D k; 

void main()
{
    color = fragColor * texture(k, uv).rgb;
}
\0";

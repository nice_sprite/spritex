use env_logger;
use log::{debug, error, info, trace};
use std::num::NonZeroU32;
use std::ops::Deref;
use winit::{
    event::{ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent},
    event_loop::EventLoop,
    window::WindowBuilder,
};

use glutin::{config::ConfigTemplateBuilder, prelude::NotCurrentGlContextSurfaceAccessor};
use glutin::{
    context::{ContextAttributesBuilder, Version},
    prelude::{GlConfig, GlDisplay},
};
use glutin_winit::{DisplayBuilder, GlWindow};
use raw_window_handle::{self, HasRawWindowHandle};

use gl::types::*;
use glutin::display::GetGlDisplay;
use glutin::prelude::*;
use glutin::surface::SwapInterval;
use std::ffi::{CStr, CString};

mod renderer;
use crate::renderer::renderer::Renderer;

fn main() {
    env_logger::init();
    info!("starting...");
    let event_loop = EventLoop::new();
    let window_builder = WindowBuilder::new()
        .with_title("gl_renderer")
        .with_transparent(true);
    let gl_version = glutin::context::ContextApi::OpenGl(Some(Version::new(3, 3)));
    let display_builder = DisplayBuilder::new().with_window_builder(Some(window_builder));
    let template = ConfigTemplateBuilder::new()
        .with_alpha_size(8)
        .with_transparency(true);

    let (mut window, gl_config) = display_builder
        .build(&event_loop, template, |configs| {
            configs
                .reduce(|accum, config| {
                    let trans_check = config.supports_transparency().unwrap_or(false)
                        & !accum.supports_transparency().unwrap_or(false);
                    if trans_check || config.num_samples() > accum.num_samples() {
                        config
                    } else {
                        accum
                    }
                })
                .unwrap()
        })
        .unwrap();

    info!("picked config with {} samples", gl_config.num_samples());
    let raw_handle = window.as_ref().map(|window| window.raw_window_handle());

    // glutin tries by default to create a openGL core context
    let gl_attributes = ContextAttributesBuilder::new()
        .with_debug(true)
        .with_context_api(gl_version)
        .build(raw_handle);

    let mut not_current_gl_context = Some(unsafe {
        gl_config
            .display()
            .create_context(&gl_config, &gl_attributes)
            .expect("failed to create context")
    });

    let mut state = None;
    let mut renderer = None;
    event_loop.run(move |event, window_target, control_flow| {
        control_flow.set_wait();
        match event {
            Event::Resumed => {
                let window = window.take().unwrap_or_else(|| {
                    let window_builder = WindowBuilder::new().with_transparent(true);
                    glutin_winit::finalize_window(window_target, window_builder, &gl_config)
                        .unwrap()
                });

                let attrs = window.build_surface_attributes(<_>::default());

                let gl_surf = unsafe {
                    gl_config
                        .display()
                        .create_window_surface(&gl_config, &attrs)
                        .unwrap()
                };

                let gl_context = not_current_gl_context
                    .take()
                    .unwrap()
                    .make_current(&gl_surf)
                    .unwrap();

                // can initialize renderer here.
                renderer.get_or_insert_with(|| Renderer::new(&gl_config.display()));

                // Try setting vsync.
                if let Err(res) = gl_surf
                    .set_swap_interval(&gl_context, SwapInterval::Wait(NonZeroU32::new(1).unwrap()))
                {
                    error!("Error setting vsync: {res:?}");
                }
                assert!(state.replace((gl_context, gl_surf, window)).is_none());
            }
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::KeyboardInput {
                    input:
                        KeyboardInput {
                            state: ElementState::Pressed,
                            virtual_keycode: Some(VirtualKeyCode::Q),
                            ..
                        },
                    ..
                } => {
                    trace!("closing");
                    control_flow.set_exit();
                }
                WindowEvent::Resized(size) => {
                    trace!("resize: {:?}", size);
                    if size.width != 0 && size.height != 0 {
                        if let Some((gl_context, gl_surface, _)) = &state {
                            gl_surface.resize(
                                gl_context,
                                NonZeroU32::new(size.width).unwrap(),
                                NonZeroU32::new(size.height).unwrap(),
                            );
                            let renderer = renderer.as_ref().unwrap();
                            renderer.resize(size.width as i32, size.height as i32);
                        }
                    }
                }
                _ => (),
            },
            Event::MainEventsCleared => {}
            Event::RedrawRequested(_) => {
                trace!("redraw requested");
                if let Some((gl_context, gl_surface, window)) = &state {
                    let renderer = renderer.as_ref().unwrap();
                    renderer.draw();
                    window.request_redraw();
                    gl_surface.swap_buffers(gl_context).unwrap();
                }
            }
            _ => (),
        }
    });
}

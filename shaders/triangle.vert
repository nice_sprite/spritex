#version 100
precision mediump float;

attribute vec3 position;
attribute vec4 color;
attribute vec2 tex_coord;

varying vec3 v_color;

void main() {
    gl_Position = vec4(position, 0.0, 1.0);
    v_color = color;
}
